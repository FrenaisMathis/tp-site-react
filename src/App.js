import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home/Home";
import CocktailsByCategory from "./pages/CocktailsByCategory/CocktailsByCategory";
import Cocktail from "./pages/Cocktail/Cocktail";

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route exact path="/byCategory">
                        <CocktailsByCategory />
                    </Route>
                    <Route path="/cocktail/:cocktailId">
                        <Cocktail />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
