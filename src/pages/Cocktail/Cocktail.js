import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "../../common.css";
import Navbar from "../../features/Navbar/Navbar";
import CardDetailsComponent from "../../features/CardDetailsComponent/CardDetailsComponent";
import Footer from "../../features/Footer/Footer";
import { fetchDrinkByIdRedux } from "../../slices/drinksSlice/drinksSlice";
import { useDispatch } from "react-redux";
import cocktailLogo from "../../assets/giphy.gif";

const Cocktail = () => {

    let { cocktailId } = useParams();

    // Set Initial States

    const dispatch = useDispatch();
    const [drink, setDrink] = useState([]);
    
    useEffect(() => {

        let drinksRedux = dispatch(fetchDrinkByIdRedux(cocktailId));

        Promise.resolve(drinksRedux).then((res) => {
            setDrink(res.payload[0]);
        });

    }, []);


    /*
    * Transform ingredients from API
    */


    // If drink is loaded show component

    if (drink.lenght !== 0 && typeof drink.strInstructions !== "undefined") {

        //  Number of ingredients in recipe < || === 16 so, get ingredient and measure

        var ingredients = [];

        for (let i = 0; i < 16; i++) {

            var name = "strIngredient" + i;
            var measure = "strMeasure" + i;

            if (drink[name]) {

                // If no measure for that ingredient

                if(!drink[measure]){

                    drink[measure] = "as you want";

                }

                ingredients.push(drink[name] + " measure : " + drink[measure]);

            }

        }


        /*
        * Transform instructions from API
        */


        // If instructions is empty set default value

        if(drink.strInstructions === ""){
            instructions = "None";
        }

        // Replace "..." by "." for split it

        drink.strInstructions = drink.strInstructions.replace("...", ".");

        // Replace default list of instructions by nothing

        if(drink.strInstructions.includes("1.")){

            var regexPatern = /1. |2. |3. |4. |5. |6. |7. |8. |9. |10. |11. |12. /gi

            drink.strInstructions = drink.strInstructions.replace(regexPatern, "");

        }


        // Make array with separator '.'

        var instructions = drink.strInstructions.split(".");

        // Remove last empty element of array

        if(instructions[instructions.length - 1] === ""){
            instructions = instructions.splice(0, instructions.length - 1);
        }

        return (

            <div>
                <Navbar/>
                <CardDetailsComponent drink={drink} ingredients={ingredients} instructions={instructions}/>
                <Footer />
            </div>

        );
    } 
    
    // Else display message

    else {
        return (
            <div>
                <Navbar/>
                <div className="customContainerLoading">
                    <h2 className="pb-3">Datas is loading, please wait...</h2>
                    <img src={cocktailLogo} alt="logo" height="400" width="400"/>
                </div>
                <Footer />
            </div>
        );
    }

};

export default Cocktail;
