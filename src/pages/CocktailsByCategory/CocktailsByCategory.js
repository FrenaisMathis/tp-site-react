import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import Navbar from "../../features/Navbar/Navbar";
import Footer from "../../features/Footer/Footer";
import CardComponent from "../../features/CardComponent/CardComponent";
import FormSelectCategory from "../../features/FormSelectCategory/FormSelectCategory";
import PaginationFilterByCategory from "../../features/PaginationFilterByCategory/PaginationFilterByCategory";

import { fetchCategoriesRedux } from "../../slices/categoriesSlice/categoriesSlice";
import { fetchDrinksByCategoryRedux } from "../../slices/drinksSlice/drinksSlice";

import "../../common.css";

import cocktailLogo from "../../assets/giphy.gif";

const CocktailByCategory = () => {

    // Set Initial States

    const dispatch = useDispatch();
    const [categories, setCategories] = useState(false);
    const [drinks, setDrinks] = useState([]);
    const [visible, setVisible] = useState(0);
    const [nextVisible, setNextVisible] = useState(9);
    const [prevDisabled, setPrevDisabled] = useState(true);
    const [nextDisabled, setNextDisabled] = useState(false);
    const [paginationDisabled, setPaginationDisabled] = useState(false);
    const [chosenCategory, setChosenCategory] = useState("Ordinary Drink");

    // Load categories when component mount

    useEffect(() => {

        fetchCategories();
        filterByCategory();

    }, []);

    const fetchCategories = () => {

        let categoriesRedux = dispatch(fetchCategoriesRedux());

        Promise.resolve(categoriesRedux).then((res) =>
            setCategories(res.payload)
        );

    };

    const filterByCategory = (event = null) => {

        let category = "";

        if (event) {
            event.preventDefault();
            category = event.target.value;
            setChosenCategory(category);
        } else {
            category = "Ordinary Drink";
        }

        // Set new value of drinks

        let drinksRedux = dispatch(fetchDrinksByCategoryRedux(category));

        Promise.resolve(drinksRedux).then((res) => {

            // reset value of drinks

            setDrinks([]);

            // set new value of drinks

            setDrinks(res.payload);

            // If size of drinks === || < 9 disabled btn next

            if (res.payload.length <= 9) {
                setPrevDisabled(true);
                setPaginationDisabled(true);
            }

            // If size of drinks > 9 enabled btn next

            if (res.payload.length > 9){
                setNextDisabled(false);
                setPaginationDisabled(false);
            }

            // Set compteurs of page

            setVisible(0);
            setNextVisible(9);

        });

    };

    const loadMore = (event) => {

        event.preventDefault();

        // Get value of nextVisible updated

        let afterNextVisible = nextVisible + 9;

        // If isn't the last page

        if (!(nextVisible >= drinks.length)) {

            // Set couters page

            setVisible((prevVisible) => prevVisible + 9);
            setNextVisible((prevNextVisible) => prevNextVisible + 9);

            // If btn prev was disbaled => enabled it

            if (prevDisabled) {
                setPrevDisabled(false);
            }

        }

        // If is the last page disabled btn next

        if (afterNextVisible >= drinks.length) {
            setNextDisabled(true);
        }
    };

    const loadLess = (event) => {

        event.preventDefault();

        // Get value of prevVisible updated

        let afterPrevVisible = visible - 9;

        //If isn't the first page

        if (visible !== 0) {

            // Set counters of page

            setVisible((prevVisible) => prevVisible - 9);
            setNextVisible((prevNextVisible) => prevNextVisible - 9);

            // If btn next disabled => enabled it

            if (nextDisabled) {
                setNextDisabled(false);
            }

        }

        // If is the first page disabled btn prev

        if (afterPrevVisible === 0) {
            setPrevDisabled(true);
        }

    };

    const firstPage = () => {
        setVisible(0);
        setNextVisible(9);
        setPrevDisabled(true);
        setNextDisabled(false);
    }

    const lastPage = () => {

        // Get modulo 9 of size of drinks

        var modulo = drinks.length % 9;

        /*
        * Set counter for last page
        * Set size of drinks - 9 for visible and size of drinks for nextVisible 
        * Add 9 - modulo to have a round number of page
        */

        setVisible((drinks.length - 9) + (9 - modulo));
        setNextVisible(drinks.length + (9 - modulo));
        setPrevDisabled(false);
        setNextDisabled(true);

    }

    if (categories && drinks.length !== 0) {

        let pagination;

        if(!paginationDisabled){

            pagination = 
                <PaginationFilterByCategory
                    loadLess={loadLess}
                    loadMore={loadMore}
                    prevDisabled={prevDisabled}
                    nextDisabled={nextDisabled}
                    firstPage={firstPage}
                    lastPage={lastPage}
                    nextVisible={nextVisible}
                />;
                
        }

        return (
            <div className="pb-5">
                <Navbar />
                <FormSelectCategory
                    categories={categories}
                    filterByCategory={filterByCategory}
                    chosenCategory={chosenCategory}
                />
                <div className="row justify-content-center mt-5 ml-0 mr-0">
                    {drinks
                        .slice(visible, nextVisible)
                        .map(function (value, index) {
                            return <CardComponent drink={value} key={index} />;
                        })}
                </div>
                {pagination}
                <Footer />
            </div>
        );
    } else {
        return (
            <div>
                <Navbar/>
                <div className="customContainerLoading">
                    <h2 className="pb-3">Datas is loading, please wait...</h2>
                    <img src={cocktailLogo} alt="logo" height="400" width="400"/>
                </div>
                <Footer />
            </div>
        );
    }
};

export default CocktailByCategory;