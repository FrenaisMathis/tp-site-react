import React from "react";

const FormSelectCategory = ({categories, filterByCategory, chosenCategory}) => {
    return (
        <div className="row mt-5 ml-0 mr-0">
            <div className="col-12 text-center">
                <label className="col-12 col-md-3 col-lg-2 pr-0 text-center">
                    Select category :
                </label>
                <select
                    className="form-control col-8 col-md-4 col-lg-3 pl-0 d-inline-block"
                    onChange={filterByCategory}
                    value={chosenCategory}
                >
                    {categories.map(function (value, index) {
                        return (
                            <option value={value.strCategory} key={index}>
                                {value.strCategory}
                            </option>
                        );
                    })}
                </select>
            </div>
        </div>
    );
};

export default FormSelectCategory;
