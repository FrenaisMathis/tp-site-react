import React from "react";
import CardComponent from "../CardComponent/CardComponent";

const CocktailsRandom = ({ drinks }) => {

    // Remove last element of drinks because we want 9 elements

    drinks = drinks.splice(0,drinks.length-1);

    return (
        <div className="row justify-content-center mt-4 ml-0 mr-0 pb-5">
            {
                drinks.map(function(value, index){
                    return(
                        <CardComponent key={index} drink={value}/>
                    );
                })
            }
        </div>
    );

};

export default CocktailsRandom;
