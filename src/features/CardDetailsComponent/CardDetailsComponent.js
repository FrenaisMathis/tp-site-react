import React from "react";
import './CardDetailsComponent.css';


const CardDetailsComponent = ({drink, ingredients, instructions}) => {
    return (
        <div className="mt-5 mb-5 pb-5 customContainer">
            <div className="card col-10 offset-1 pl-3 pr-3 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                <img
                    className="card-img-top mt-3"
                    src={drink.strDrinkThumb}
                    height="200px"
                    alt={drink.strDrink}
                />
                <div className="card-body">
                    <h5 className="card-title">
                        {drink.strDrink} : {drink.strCategory}
                    </h5>
                    <h6 className="card-subtitle pb-3 pt-3">
                        List of ingredients :
                    </h6>
                    {ingredients.map(function (value, index) {
                        return <p key={value}>- {value}</p>;
                    })}
                    <h6 className="card-subtitle pb-3 pt-3">Instructions : </h6>
                    {
                        instructions.map(function (value, index) {
                            return <p key={value}>- {value}.</p>
                        })
                    }
                </div>
            </div>
        </div>
    );
};

export default CardDetailsComponent;