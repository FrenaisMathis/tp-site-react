import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const Navbar = () => {

	let { path } = useRouteMatch();

	let classNavItem = "nav-item";

	if(path === '/byCategory'){
		classNavItem = classNavItem.concat(' active') ;
	}

	return (
		<nav className="navbar navbar-expand-lg navbar-dark primary-color">
			<Link className="navbar-brand" to="/">
				Home
			</Link>
			<button
				className="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent"
				aria-expanded="false"
				aria-label="Toggle navigation"
			>
				<span className="navbar-toggler-icon" />
			</button>

			<div className="collapse navbar-collapse" id="navbarSupportedContent">
				<ul className="navbar-nav mr-auto">
					<li className={classNavItem}>
						<Link className="nav-link" to="/byCategory">
							Filter by category
						</Link>
					</li>
				</ul>
			</div>
		</nav>
	);
};

export default Navbar;
