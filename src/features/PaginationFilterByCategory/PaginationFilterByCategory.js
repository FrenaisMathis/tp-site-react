import React from 'react';

import { useState, useEffect } from "react";

const PaginationFilterByCategory = ({ loadLess, loadMore, prevDisabled, nextDisabled, firstPage, lastPage, nextVisible }) => {

    const [windowSize, setWindowSize] = useState({
        width: undefined,
    });
      
    useEffect(() => {

        function handleResize() {

            setWindowSize({
              width: window.innerWidth,
              height: window.innerHeight,
            });
        }

        window.addEventListener("resize", handleResize);
          
        setWindowSize({
            width: window.innerWidth
        });

        return () => window.removeEventListener("resize", handleResize);
        
    }, []);
    
    var numPage = nextVisible / 9;

    if(typeof windowSize.width === "undefined"){
        return null;
    }

    else if(typeof windowSize.width != "undefined" && windowSize.width < 576){
        return (
            <div className="row ml-0 pb-5 mr-0">
                <div className="text-center col-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
                    <div className="float-left d-inline col-12">
                        <button className="btn btn-primary" onClick={firstPage} disabled={prevDisabled}>
                            First
                        </button>
                    </div>
                    <div className="float-left d-inline">
                        <button className="btn btn-primary" onClick={loadLess} disabled={prevDisabled}>
                            &lt;
                        </button>
                    </div>
                    <p className="d-inline-block pt-3">Page {numPage}</p>
                    <div className="float-right d-inline">
                        <button className="btn btn-primary" onClick={loadMore} disabled={nextDisabled}>
                            &gt;
                        </button>
                    </div>
                    <div className="float-right d-inline col-12">
                        <button className="btn btn-primary" onClick={lastPage} disabled={nextDisabled}>
                            Last
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    else{
        return (
            <div className="row ml-0 pb-5 mr-0">
                <div className="text-center col-12 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
                    <div className="float-left d-inline">
                        <button className="btn btn-primary" onClick={firstPage} disabled={prevDisabled}>
                            First
                        </button>
                        <button className="btn btn-primary" onClick={loadLess} disabled={prevDisabled}>
                            &lt;
                        </button>
                    </div>
                    <p className="d-inline-block pt-3">Page {numPage}</p>
                    <div className="float-right d-inline">
                        <button className="btn btn-primary" onClick={loadMore} disabled={nextDisabled}>
                            &gt;
                        </button>
                        <button className="btn btn-primary" onClick={lastPage} disabled={nextDisabled}>
                            Last
                        </button>
                    </div>
                </div>
            </div>
        );
    }
    
};

export default PaginationFilterByCategory;